import concatCollectionValues from "../../src/helpers/concat-collection-values/index";

describe("should test concatCollectionValues", () => {
    test("and return valid value", () => {
        const expecting = "one, two, three";
        const collection = [
            {
                name: "one"
            },
            {
                name: "two"
            },
            {
                name: "three"
            }
        ];

        expect(concatCollectionValues(collection, "name", ", ")).toBe(expecting);
    });
});
