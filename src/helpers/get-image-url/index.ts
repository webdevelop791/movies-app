import { IMAGE_HOST } from "../../config";

const getImageUrl = (name: string, width: number) => `${IMAGE_HOST}w${width}/${name}`;

export default getImageUrl;
