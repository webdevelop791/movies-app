const concatCollectionValues = (collection: Array<any>, prop: string, symbol: string) =>
    collection.map((item: any) => item[prop]).join(symbol);

export default concatCollectionValues;
