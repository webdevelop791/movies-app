import * as React from "react";
import { Layout, Menu } from "antd";
import * as classNames from "classnames";
import { Link } from "react-router-dom";
import { HOME_PAGE, MOVIES_LIST } from "../../routes/constants";
import Search from "../search";
import * as styles from "./styles/index.less";
import "../../styles/common/index.less";

const { Header, Content, Footer } = Layout;
const cx = classNames.bind(styles);
const { Item } = Menu;

class PageWrap extends React.Component {
    render() {
        return (
            <Layout>
                <Header className={cx("header-row")}>
                    <Menu
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={[HOME_PAGE]}
                        style={{ lineHeight: "64px" }}
                    >
                        <Item key={HOME_PAGE}>
                            <Link to={HOME_PAGE}>Home</Link>
                        </Item>
                        <Item key={MOVIES_LIST}>
                            <Link to={MOVIES_LIST}>Movies</Link>
                        </Item>
                    </Menu>
                    <Search />
                </Header>
                <Content style={{ padding: "0 50px", marginTop: 64 }}>
                    <div style={{ background: "#fff", padding: 24, minHeight: 380 }}>
                        {this.props.children}
                    </div>
                </Content>
                <Footer style={{ textAlign: "center" }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        );
    }
}

export default PageWrap;
