import * as React from "react";
import * as classNames from "classnames";
import { Dispatch } from "redux";
import * as _ from "lodash";
import { connect } from "react-redux";
import { Input } from "antd";
import { searchMovies } from "../../containers/movies/list/actions";
import * as styles from "./styles/index.less";
import SearchResults from "./search-results";
import { MovieInterface } from "../../containers/movies/common/types";

const cx = classNames.bind(styles);

interface SearchInterface {
    dispatch: Dispatch;
    movies: {
        search: {
            results: Array<MovieInterface>;
        };
    };
}

class Search extends React.Component<SearchInterface> {
    search = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { value } = e.target;

        if (value !== "") {
            this.props.dispatch(searchMovies({ query: value }));
        }
    };

    render() {
        const { results } = this.props.movies.search;

        return (
            <div className={cx("search")}>
                <div className={cx("search-input")}>
                    <Input placeholder="Search movie..." onChange={this.search} />
                </div>
                {results.length ? <SearchResults data={results} /> : null}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    movies: state.movies
});

export default connect(mapStateToProps)(Search);
