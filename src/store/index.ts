import "regenerator-runtime/runtime";
import { createStore, applyMiddleware } from "redux";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import reducers from "../reducers";
import sagas from "../sagas";

export default function configureStore(): any {
    const logger = createLogger();
    const sagaMiddleware = createSagaMiddleware();
    const middlewares = [logger, sagaMiddleware];
    const store = createStore(reducers, applyMiddleware(...middlewares));

    sagaMiddleware.run(sagas);

    return store;
}
