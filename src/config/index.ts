export const PROTOCOL: string = "https://";
export const HOST: string = "api.themoviedb.org/3";
export const API_KEY: string = "201329e72797698e1f3fa14e96156c79";
export const API_URL: string = `${PROTOCOL}${HOST}`;
export const IMAGE_HOST: string = "http://image.tmdb.org/t/p/";
export const LANG: string = "en-EN";
