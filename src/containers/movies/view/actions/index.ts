import { MovieInterface } from "../../common/types";

export const MODULE_URL = "/movie";

export const getMovieRecommendationsURL = (id: number) => `/movie/${id}/recommendations`;
export const getMovieSimilarURL = (id: number) => `/movie/${id}/similar`;

export const FETCH_MOVIE = "/movie/fetch";
export const fetchMovie = (payload: number) => ({
    type: FETCH_MOVIE,
    payload
});

interface FetchSuccessPayload {
    data: MovieInterface;
    recommendations: Array<MovieInterface>;
    similar: Array<MovieInterface>;
}

export const FETCH_MOVIE_SUCCESS = "/movie/fetchSuccess";
export const fetchMovieSuccess = (payload: FetchSuccessPayload) => ({
    type: FETCH_MOVIE_SUCCESS,
    payload
});

export const FETCH_MOVIE_FAILURE = "/movie/fetchFailure";
export const fetchMovieFailure = (payload: any) => ({
    type: FETCH_MOVIE_FAILURE,
    payload
});
