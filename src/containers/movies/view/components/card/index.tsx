import * as React from "react";
import { Row, Col, Rate, List } from "antd";
import * as classNames from "classnames";
import { Link } from "react-router-dom";
import styles from "./styles/index.less";
import getImageUrl from "../../../../../helpers/get-image-url";
import concatColllectionValues from "../../../../../helpers/concat-collection-values";
import { MovieInterface } from "../../../common/types";

const posterWidth = 500; //"w92", "w154", "w185", "w342", "w500", "w780", or "original"
const cx = classNames.bind(styles);
const getRate = rate => Math.round((rate / 2) * 2) / 2;

interface Props {
    movie: {
        data: MovieInterface;
        recommendations: Array<MovieInterface>;
        similar: Array<MovieInterface>;
    };
}

const MovieCard = ({ movie }: Props) => (
    <Row className={cx("movie")}>
        <Col span={8} className={cx("movie-image")}>
            <img src={getImageUrl(movie.data.poster_path, posterWidth)} />
        </Col>
        <Col span={15} offset={1}>
            <div className={cx("movie-title")}>
                <h1>{movie.data.original_title}</h1>
            </div>
            <div className={cx("movie-genres")}>
                {concatColllectionValues(movie.data.genres, "name", ", ")}
            </div>
            <div className={cx("movie-description")}>{movie.data.overview}</div>
            <div className={cx("movie-rate")}>
                <Rate disabled defaultValue={getRate(movie.data.vote_average)} count={5} allowHalf={true} />
            </div>

            <Row gutter={{ lg: 20 }} className={cx("movie-recommendations")}>
                <Col span={12}>
                    <h2>Recommendations</h2>
                    <List
                        size="small"
                        bordered
                        dataSource={movie.recommendations}
                        renderItem={item => (
                            <List.Item>
                                <Link to={`/movies/${item.id}`}>{item.original_title}</Link>
                            </List.Item>
                        )}
                    />
                </Col>
                <Col span={12}>
                    <h2>Similar</h2>
                    <List
                        size="small"
                        bordered
                        dataSource={movie.similar}
                        renderItem={item => (
                            <List.Item>
                                <Link to={`/movies/${item.id}`}>{item.original_title}</Link>
                            </List.Item>
                        )}
                    />
                </Col>
            </Row>
        </Col>
    </Row>
);

export default MovieCard;
