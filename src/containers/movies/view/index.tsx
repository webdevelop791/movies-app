import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { fetchMovie } from "./actions";
import MovieCard from "./components/card";
import { MovieInterface } from "../common/types";

interface Props {
    match: {
        params: {
            id: number;
        };
    };
    dispatch: Dispatch;
    movie: {
        data: MovieInterface;
        recommendations: Array<MovieInterface>;
        similar: Array<MovieInterface>;
    };
}

class MovieView extends React.Component<Props> {
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.dispatch(fetchMovie(id));
    }

    componentWillReceiveProps(nextProps) {
        const prevId = this.props.match.params.id;
        const nextId = nextProps.match.params.id;

        if (prevId !== nextId) {
            this.props.dispatch(fetchMovie(nextId));
        }
    }

    render() {
        const { movie } = this.props;

        return movie.data.id ? <MovieCard movie={movie} /> : null;
    }
}

const mapStateToProps = state => ({
    movie: state.movie
});

export default connect(mapStateToProps)(MovieView);
