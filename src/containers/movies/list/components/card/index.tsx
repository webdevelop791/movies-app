import * as React from "react";
import { Card, Tooltip } from "antd";
import * as classNames from "classnames";
import { Link } from "react-router-dom";
import styles from "./styles/index.less";
import getImageUrl from "../../../../../helpers/get-image-url";
import { MovieInterface } from "../../../common/types";

const cx = classNames.bind(styles);
const posterWidth = 342; //"w92", "w154", "w185", "w342", "w500", "w780", or "original"

interface Props {
    movie: MovieInterface;
}

const MovieCard = ({ movie }: Props) => (
    <Card className={cx("movie-card")}>
        <div className={cx("movie-card__image")}>
            <img src={getImageUrl(movie.poster_path, posterWidth)} />
        </div>
        <div className={cx("movie-card__title")}>
            <Tooltip placement="bottom" title={movie.original_title}>
                <Link to={`/movies/${movie.id}`}>{movie.original_title}</Link>
            </Tooltip>
        </div>
    </Card>
);

export default MovieCard;
