import axios, { AxiosInstance } from "axios";
import { API_URL, API_KEY } from "../../config";

interface Props extends AxiosInstance {
    params?: {
        api_key: string;
    };
}

class Api {
    protected instance: Props;

    constructor() {
        this.instance = axios.create({
            baseURL: API_URL,
            headers: {
                "Content-Type": "application/json;charset=utf-8"
            },
            params: {
                api_key: API_KEY
            }
        });
    }

    fetch = (url, queryParams?) => {
        const params = { ...this.instance.params, ...queryParams };

        return this.instance.get(url, { params }).then(response => response.data);
    };
}

export default new Api();
